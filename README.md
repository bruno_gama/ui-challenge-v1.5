# Avenue Code UI Test - Bruno Gama's submission

## Project dependencies

This project should run fine in any operational system (although it has been
tested only on Linux and Windows). The required software is:

* [Node.js](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/en/)
* [Docker and docker-compose](https://www.docker.com/) (Optional)

## Running the project

### **Important**: freegeoip.net is deprecated

The API should stop working in July first, this project is using the new
endpoint from [IPStack](https://ipstack.com/). Which requires
extra configuration.

### Configure API Key

You need to create an account at [IPStack](https://ipstack.com/) in order to
retrieve an API key (or copy mine from the following code block).

Create an `.env` file in the project's root based on `.env.sample` and add your
API Key, as in

```env
IPSTACK_API_KEY=7651023d7df529d4e4d5652319760905
```

The above Key is from my personal account, feel free to use it.

### Running with Docker

The quickest way to get the project up and running in production mode is with
docker-compose:

```bash
docker-compose up
```

This will build a Node.js container, bundle the application in production mode,
and serve the website at http://localhost:8080

### Running without Docker

Install all dependencies with Yarn:

```bash
yarn install
```

#### Development mode

Run the following command to run a basic webpack-dev-server.

```bash
yarn start
```

#### Production mode

Run the following command to bundle a production version of the application
and run a basic server with express.js.

```bash
yarn serve
```

## About the methodology

### React

I opted for using React in place of another popular framework such as Angular or
Vue mainly for style preferences and development phylosophy affinity.

React encourages a functional programming style that avoids mutability and
allows the developer to pick libraries based on exactly the type of
functionality and style they are aiming for.

### No-Redux

I am a **huge** Redux fanboy and, but I am also a part of the
[you-might-not-need-redux](https://medium.com/@dan_abramov/you-might-not-need-redux-be46360cf367)
crew and did not see any opportunity to apply it to thiswith any advantage
except making things look more complicated than they are. I believe modern React 
development needs to be able to develop solutions quickly without the need
of bigger state management libraries and tried to show this with this project.

### Flow

I have a strong preference for working with types, both for documentation and
for providing safer applications. I picked Flow in place of TypeScript because
it is very easy to apply to any project, and lets me pick exacly which parts of
my project I want to type-check, and how strict I want my validatation to be.

### ESLint

This project comes with a basic ESLint configuration that heavily relies on
imported recommended  configs and plugins, along with a simple Prettier
config file.

### Styled Components

There are a few advantages for following the css-in-js approach, and
Styled-Components is currently my favorite implementation. It handles all the
old CSS issues such as global namespaces and dead code elimination while still
letting me work with all the freedom that I used to have from SCSS.

The choice of not using a design-system framework such as Bulma or Material
Design comes partially from my intention of demonstrating that I know how to
design a CSS-in-JS project and the time constraints that didnt allow me to set
up a sensible project structure that would take real advantage of such framework
while maintaining the freedom from styled-components.

### Webpack

I opted for using a custom Webpack configuration in place of `create-react-app`
in order to have more configuration freedom, and also to demonstrate that I can
understand how Webpack works.

### Unit Tests

This project uses Jest as its framework for unit tests and regression tests.
The project aims to impose more exhaustive specifications with BDD-style unit
tests for data-related helper funcion, higher-order components and "smart"
container components, while using simpler regression (snapshot) tests for 
the more complex  presentational components. Purely presentation primitive
components don't necessarily warrant testing.

Some components and functions havent been added to the test coverage due to time
constraints.

## Room for growth

There are obviously a lot of things I wish that were part of this project
but could not have been applied within the available time, some of the ideas
that could a nice addition are:

* A "search history" kind of feature, where the user could select one of his old queries
  * This would be a nice opportunity to use Redux along with a persistence solution that uses localStorage or indexedDB
* A better looking user-interface, I would probably use Bulma as a design system and extend some of it's elements
* More specific user feedback, such as messages that tell the user that the API has failed to find the website, or is offline
* Basic Service Workers and PWA capabilities (e.g. app icon, fullscreen, offline functionality)
* Better docker usage (such as running webpack-dev-server or uploading the bundle to an AWS S3 bucket)












