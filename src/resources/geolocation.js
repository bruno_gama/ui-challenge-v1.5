// @flow strict
/* global process */
import axios from 'axios'

const ENDPOINT = 'http://api.ipstack.com'

const KEY = process.env.IPSTACK_API_KEY || ''

const get = () =>
  axios
    .get(`${ENDPOINT}/check?access_key=${KEY}`)
    .then(response => response.data)

type WebsiteParams = { url: string }
const getWebsite = ({ url }: WebsiteParams) =>
  axios
    .get(`${ENDPOINT}/${url}?access_key=${KEY}`)
    .then(response => response.data)

export const geolocation = { get, getWebsite }
