import React from 'react'
import styled from 'styled-components'

const Container = styled.header`
  text-align: center;
  background: ${({ theme }) => theme.headerColor};
  padding: 20px 0;
  margin-bottom: 40px;
  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.3);
`

const Title = styled.h1`
  color: #fff;
  font-weight: bold;
`

const Subtitle = styled.h2`
  color: #fff;
  font-weight: bold;
`

const Header = () => (
  <Container>
    <Title>Geolocation Test</Title>
    <Subtitle>Submited by Bruno Gama</Subtitle>
  </Container>
)

export default Header
