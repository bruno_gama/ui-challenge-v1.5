// @flow strict
import React from 'react'
import styled from 'styled-components'

import { validateUrl } from '../../../helpers/validateUrl.js'
import { Button } from '../../primitives/Button'

const Container = styled.div`
  width: 100%;
  padding: 0 10px;
`

const Form = styled.form`
  width: 100%;
  display: flex;
  flex-direction: row;
`

const Field = styled.input`
  flex: 1;
  border: 1px solid #ccc;
  border-right: 0;
  border-radius: 5px 0 0 5px;
`

const SubmitButton = Button.extend`
  flex: 0 0 140px;
  border-radius: 0 5px 5px 0;
  border-left: 0;
`

const Message = styled.p`
  width: 100%;
  background: ${({ theme }) => theme.danger};
  border: 1px solid ${({ theme }) => theme.danger};
  color: #white;
  font-weight: bold;
  font-size: 12px;
  border-radius: 5px;
  text-align: center;
  margin-top: 10px;
`

type Props = { onSubmit: (value: string) => void }
type State = { value: string, valid: boolean }

class WebsiteForm extends React.PureComponent<Props, State> {
  state = { value: '', valid: true }

  handleChange = ({ target }: SyntheticInputEvent<Text>) => {
    this.setState({ value: target.value })
  }

  handleSubmit = (event: SyntheticEvent<>) => {
    event.preventDefault()

    if (validateUrl(this.state.value)) {
      this.setState({ valid: true })
      this.props.onSubmit(this.state.value)
    } else {
      this.setState({ valid: false })
    }
  }

  render() {
    const { value, valid } = this.state

    return (
      <Container>
        <Form onSubmit={this.handleSubmit}>
          <Field
            size="1"
            type="text"
            onChange={this.handleChange}
            value={value}
          />
          <SubmitButton type="submit">Website Location</SubmitButton>
        </Form>
        {!valid ? <Message>Please type a valid website domain</Message> : null}
      </Container>
    )
  }
}

export default WebsiteForm
