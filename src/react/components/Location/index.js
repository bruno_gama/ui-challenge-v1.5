// @flow strict
import React from 'react'
import styled from 'styled-components'

import InfoButton from './InfoButton'
import type { LocationData } from '../../../types/location.js'

type Props = {
  location: LocationData,
  host?: string,
  resourceError: {
    status: number,
  },
}

type State = {
  date: string,
}

type alertParams = { text: string }

const Table = styled.ul`
  padding: 10px;
`

const Row = styled.li`
  display: flex;
  flex-direction: row;
  height: 32px;
  line-height: 32px;
`

const FieldName = styled.span`
  flex: 0 0 90px;
`

const Value = styled.span`
  flex: 1;
  text-align: center;
`

const Info = styled.span`
  flex: 0 0 50px;
  text-align: right;
`

class Location extends React.PureComponent<Props, State> {
  state = { date: new Date().toString() }

  showAlert = ({ text }: alertParams) => {
    alert(text)
  }

  render() {
    const { location, resourceError, host } = this.props
    const { date } = this.state

    if (resourceError) {
      return (
        <p>
          There has been an error fetching the requested geolocation, please try
          again.
        </p>
      )
    } else if (!location) {
      return <p>loading...</p>
    } else {
      const rows = [
        { field: 'IP', value: location.ip },
        { field: 'Country', value: location.country_name },
        { field: 'Region', value: location.region_name },
        { field: 'City', value: location.city },
        { field: 'Zip Code', value: location.zip },
        { field: 'Latitude', value: location.latitude },
        { field: 'Longitude', value: location.longitude },
      ]

      return (
        <Table>
          {rows.map((row, i) => (
            <Row key={i}>
              <FieldName>{row.field}</FieldName>
              <Value>{row.value}</Value>
              <Info>
                <InfoButton
                  field={row.field}
                  date={date}
                  onClick={this.showAlert}
                  host={host}
                />
              </Info>
            </Row>
          ))}
        </Table>
      )
    }
  }
}

export default Location
