// @flow strict
import React from 'react'

import { Button } from '../../../primitives/Button'

const RoundButton = Button.extend`
  border-radius: 50%;
  width: 25px;
  height: 25px;
  font-size: 14px;
  line-heigt: 25px;
`

type Props = {
  field: string,
  date: string,
  onClick: ({ text: string }) => void,
  host?: string,
}

class InfoButton extends React.PureComponent<Props> {
  handleClick = () => {
    const { field, date, host, onClick } = this.props

    if (host) {
      onClick({
        text: `This is ${host}'s ${field} according to freegeoip.net at ${date}`,
      })
    } else {
      onClick({
        text: `This is your ${field} according to freegeoip.net at ${date}`,
      })
    }
  }

  render() {
    return <RoundButton onClick={this.handleClick}>?</RoundButton>
  }
}

export default InfoButton
