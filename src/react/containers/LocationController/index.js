import React from 'react'
import styled from 'styled-components'

import { withResource } from '../../HOCs/withResource'
import Location from '../../components/Location'
import { geolocation } from '../../../resources/geolocation.js'
import WebsiteForm from '../../components/WebsiteForm'
import { Button } from '../../primitives/Button'

const UserLocation = withResource(geolocation.get, 'location')(Location)
const WebsiteLocation = withResource(geolocation.getWebsite, 'location')(
  Location
)

const Container = styled.section`
  max-width: 800px;
  margin: 0 auto;

  @media (min-width: 720px) {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
  }
`

const LocationBox = styled.article`
  width: 100%;
  max-width: 380px;
  min-height: 380px;
  margin: 20px auto;
  background: ${({ theme }) => theme.boxColor};
  padding: 10px;
  border-radius: 5px;
  color: #fff;

  @media (min-width: 720px) {
    max-width: 45%;
    flex: 1;
  }
`

const LocationHeader = styled.header`
  flex: 0 0 30px;
`

const LocationControls = styled.nav`
  display: flex;
  align-items: flex-start;
  flex-direction: row;
  height: 60px;

  > button {
    flex: 1;
    margin: 0 0.5em;
  }
`

const Title = styled.h3`
  font-size: 20px;
  line-height: 48px;
  font-weight: bold;
  text-align: center;
`

class LocationController extends React.PureComponent {
  state = {
    showUserLocation: false,
    websiteLocation: null,
  }

  showUserLocation = () => {
    this.setState({ showUserLocation: true })
  }

  resetUserLocation = () => {
    this.setState({ showUserLocation: false })
  }

  onWebsiteSubmit = value => {
    this.setState({ websiteLocation: value })
  }

  render() {
    return (
      <Container>
        <LocationBox>
          <LocationHeader>
            <Title>Estimated user location</Title>
          </LocationHeader>

          <LocationControls>
            <Button onClick={this.showUserLocation}>My Location</Button>
            <Button onClick={this.resetUserLocation}>Clear Location</Button>
          </LocationControls>

          <If condition={this.state.showUserLocation}>
            <UserLocation />
          </If>
        </LocationBox>

        <LocationBox>
          <LocationHeader>
            <Title>Estimated website location</Title>
          </LocationHeader>

          <LocationControls>
            <WebsiteForm onSubmit={this.onWebsiteSubmit} />
          </LocationControls>

          <If condition={this.state.websiteLocation}>
            <WebsiteLocation
              params={{ url: this.state.websiteLocation }}
              host={this.state.websiteLocation}
            />
          </If>
        </LocationBox>
      </Container>
    )
  }
}

export default LocationController
