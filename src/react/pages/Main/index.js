// @flow
import React from 'react'
import styled from 'styled-components'

import Header from '../../components/Header'
import LocationController from '../../containers/LocationController'

const Container = styled.div`
  font-family: 'Open Sans';
`

const Main = () => (
  <Container>
    <Header />
    <LocationController />
  </Container>
)

export default Main
