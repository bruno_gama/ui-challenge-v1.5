//@flow strict
import styled from 'styled-components'

export const Button = styled.button`
  background: ${({ theme }) => theme.primaryColor};
  color: #fff;
  border: none;
  border-radius: 5px;
  font-size: 16px;
  padding: 0.2em 0.4em;
  border: 1px solid #ccc;
  cursor: pointer;
  transition: background 0.2s linear;

  &:hover {
    background: ${({ theme }) => theme.hoverColor};
  }
`
