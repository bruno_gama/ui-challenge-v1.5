// @flow strict
import { match } from 'ramda'

export const validateUrl = (url: string) =>
  match(/^(www\.)?\w+\.com(\.br)?$/g, url).length > 0
