import { validateUrl } from '../../src/helpers/validateUrl.js'

describe('validateUrl()', () => {
  test('returns true according to acceptance criteria', () => {
    expect(validateUrl('')).toBe(false)
    expect(validateUrl(' ')).toBe(false)
    expect(validateUrl('123456')).toBe(false)
    expect(validateUrl('!')).toBe(false)
    expect(validateUrl('www.google.com')).toBe(true)
    expect(validateUrl('duckduckgo.com')).toBe(true)
    expect(validateUrl('http://www.g1.com')).toBe(false)
    expect(validateUrl('subdomain.avenuecode.com')).toBe(false)
  })
})
