import React from 'react'
import { shallow, mount } from 'enzyme'

import InfoButton from '../../../../src/react/components/Location/InfoButton'

describe.only('InfoButton', () => {
  test('renders', () => {
    expect(
      shallow(
        <InfoButton field={'test'} date={'test date'} onClick={() => null} />
      )
    ).toBeDefined()
  })

  test('fires passed function on click', () => {
    const onClick = jest.fn()

    mount(<InfoButton field={'test'} date={'test date'} onClick={onClick} />)
      .find('button')
      .simulate('click')

    expect(onClick).toHaveBeenCalledWith({
      text: 'This is your test according to freegeoip.net at test date',
    })
  })
})
