import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Location from '../../../../src/react/components/Location'

const props = {
  ip: '123.456.789-10',
  type: 'type',
  continent_code: 'ABC',
  continent_name: 'Testgaea',
  country_code: 'XYZ',
  country_name: 'Test Nation',
  region_code: 'FFF',
  region_name: 'Testlands',
  city: 'Teste do Sul',
  zip: '',
  latitude: -10,
  longitude: 10,
  location: {
    geoname_id: 12321321,
    capital: 'AA',
    languages: [
      {
        code: 'BB',
        name: 'Testese',
        native: 'Testês',
      },
    ],
    country_flag: '',
    country_flag_emoji: '',
    country_flag_emoji_unicode: '',
    calling_code: '+55',
    is_eu: false,
  },
}

describe.only('Location', () => {
  test('renders correctly', () => {
    const component = shallow(<Location {...props} />)

    expect(toJson(component)).toMatchSnapshot()
  })
})
